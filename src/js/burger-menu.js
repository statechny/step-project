export {toggleBurgerClass}
function toggleBurgerClass () {
    const burger = document.querySelector('.menu-btn');
    burger.addEventListener('click', () => {
        burger.classList.toggle('menu-btn_active');
        document.getElementById('topMenu').classList.toggle('menu-bg');
    })
};